(function () {
	var Prapptice = angular.module('Prapptice', [
		'ui.router',
		'EntryModule',
		'GuitarModule',
		'EffectModule',
		'AmpModule',
		'SightReadingModule'
	]);

	Prapptice.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: '../partials/home.html'
			})
			.state('signup', {
				url: '/signup',
				templateUrl: '../partials/signup.html'
			})
			.state('login', {
				url: '/login',
				templateUrl: '../partials/login.html'
			});
		$urlRouterProvider.otherwise('/');

		$locationProvider.html5Mode(true);
	});

	Prapptice.run(['$rootScope', '$location', '$window', function ($rootScope, $location, $window) {
		// TODO: Find better way to do this.  With $rootScope conditionals on the UI, there's a flash of those hidden things when refreshing the page
		localStorage.getItem('access_token') ? $rootScope.hasToken = true : $rootScope.hasToken = false;

		// Include Google Analytics
		$window.ga('create', 'UA-77798731-3', 'auto');
		$window.ga('require', 'linkid');

		$rootScope.$on('$stateChangeSuccess', function (event) {
			$window.ga('send', 'pageview', $location.path());
		});
	}]);

	Prapptice.controller('SignupController', ['$scope', '$http', '$state', '$window', function ($scope, $http, $state, $window) {
		$scope.signup = function (user) {
			user.show_name = false;
			$http({
				method: 'POST',
				data: user,
				url: '/api/user'
			}).then(function successCallback() {
				$window.ga('send',
					'event',
					'User',
					'Registration',
					'New User Registration');
				$state.go('login');
			}, function errorCallback(response) {
				console.error(response);
			});
		};
	}]);

	Prapptice.controller('HomeController', ['$scope', '$http', function($scope, $http) {
		$http({
			method: 'GET',
			url: '/api/entry/hours'
		}).then(function successCallback(response) {
			$scope.hours = response.data.hours;
		}, function errorCallback(response) {
			console.error(response);
		});
	}]);
	
	Prapptice.controller('LoginController', ['$scope', '$rootScope', '$http', '$state', '$window', function ($scope, $rootScope, $http, $state, $window) {
		$scope.login = function (userInfo) {
			// Grabs tokens to use on subsequent requests
			userInfo.grant_type = "password";
			userInfo.client_id = "prappticeapiv1";
			userInfo.client_secret = "PrappticeApiVersion1";
			$http({
				method: 'POST',
				data: userInfo,
				url: '/api/oauth/token'
			}).then(function successCallback(response) {
				localStorage.setItem("access_token", response.data.access_token);
				localStorage.setItem("refresh_token", response.data.refresh_token);
				localStorage.setItem("token_type", response.data.token_type);
				$rootScope.hasToken = true;
				$window.ga('send',
					'event',
					'User',
					'Login',
					'User Login');
				$state.go('home');
			}, function errorCallback(response) {
				console.error(response);
			});
		};
		
		$scope.logout = function () {
			// Deletes local storage making user's tokens ineffective
			localStorage.removeItem("access_token");
			localStorage.removeItem("refresh_token");
			localStorage.removeItem("token_type");
			$rootScope.hasToken = false;
			$state.go('login');
		};
	}]);

	Prapptice.directive('loadingImage', function() {
		return {
			restrict: 'E',
			scope: {
				itemToWatch: '='
			},
			template: '<div class="col-xs-12" ng-if="!itemToWatch" style="text-align:center;">' +
			'<img src="/images/spinner.gif" alt="Loading..." style="width:25%;"/>' +
			'</div>'
		}
	});

})();
