(function () {

	var SightReading = angular.module('SightReadingModule', []);
	
	SightReading.config(function ($stateProvider) {
		$stateProvider
			.state('notes', {
				url: '/sight-reading/notes',
				templateUrl: '../../partials/sight-reading/notes.html'
			});
	});

	SightReading.controller('SightReadingController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
		// Give us an object config we can use to send to API
		$scope.sightReaderConfig = {
			sharps: false,
			flats: false,
			frequency: 1,
			numberOfNotes: 96,
			// Major or minor key
			key: 'Cmaj',
			// range for sight reading
			bounds: ['C4','B4'],
			// treble, bass, tenor, alto, etc.
			clef: 'treble'
		};
		// Since we're not binding to something on the template, we'll use this to use the animation on initial load
		$scope.sightReaderIsLoaded = null;
		function generateNotes() {
			// Send info about the sight reading generation to Google Analytics
			$window.ga('send',
				'event',
				'Sight Reading',
				'Generate Notes',
				'sharps: ' + $scope.sightReaderConfig.sharps +
				' flats: ' + $scope.sightReaderConfig.flats +
				' number of notes: ' + $scope.sightReaderConfig.numberOfNotes +
				' frequency: ' + $scope.sightReaderConfig.frequency +
				' clef: ' + $scope.sightReaderConfig.clef);
			$http({
				method: 'POST',
				url: 'api/sight-reading/notes/',
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				},
				data: {
					key: $scope.sightReaderConfig.key,
					numberOfNotes: $scope.sightReaderConfig.numberOfNotes,
					sharps: $scope.sightReaderConfig.sharps,
					flats: $scope.sightReaderConfig.flats,
					frequency: $scope.sightReaderConfig.frequency,
					bounds: $scope.sightReaderConfig.bounds,
					clef: $scope.sightReaderConfig.clef
				}
			}).then(function successCallback(response) {
				// Set this variable so we can get the loading animation to go away
				$scope.sightReaderIsLoaded = true;
				ABCJS.renderAbc('notation', response.data.message);
			}, function errorCallback(response) {
				console.error(response);
			});
		}

		generateNotes();

		$scope.update = function () {
			generateNotes();
		};

	}]);
	
})();
