(function () {

	var Amp = angular.module('AmpModule', []);

	Amp.config(function ($stateProvider) {
		$stateProvider
			.state('amps', {
				url: '/gear/amps',
				templateUrl: '../../partials/gear/amp.html'
			})
			.state('new-amp', {
				url: '/gear/amp/new',
				templateUrl: '../../partials/gear/new-amp.html'
			})
			.state('edit-amp', {
				url: '/gear/amp/edit/:id',
				templateUrl: '../../partials/gear/edit-amp.html',
				resolve: {
					promiseObject: function ($http, $stateParams) {
						return $http({
							method: 'GET',
							url: '/api/gear/amps/' + $stateParams.id,
							headers: {
								'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
							}
						}).then(function successCallback(response) {
							return response.data;
						});
					}
				},
				controller: function ($scope, promiseObject) {
					$scope.selectedAmp = promiseObject;
				}
			});
	});

	Amp.controller('AmpController', ['$scope', '$http', '$state', '$stateParams', '$window', function ($scope, $http, $state, $stateParams, $window) {
		$http({
			method: 'GET',
			url: '/api/gear/amps',
			headers: {
				'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
			}
		}).then(function successCallback(response) {
			$window.ga('send', 'event', 'Gear Vault', 'List', 'Amps');
			$scope.amps = response.data.amps;
		}, function errorCallback(response) {
			console.error(response);
		});

		$scope.post = function (amp) {
			$http({
				method: 'POST',
				data: amp,
				url: '/api/gear/amps',
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Create', 'Amp');
				$state.go('amps');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.put = function (amp) {
			$http({
				method: 'PUT',
				data: amp,
				url: '/api/gear/amps/' + $stateParams.id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Update', 'Amp');
				$state.go('amps');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.delete = function (amp, index) {
			$http({
				method: 'DELETE',
				url: '/api/gear/amps/' + amp._id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Remove', 'Amp');
				$scope.amps.splice([index], 1);
			}, function errorCallback(response) {
				console.error(response);
			});
		};
	}]);

})();
