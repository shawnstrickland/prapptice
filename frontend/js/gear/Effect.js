(function () {

	var Effect = angular.module('EffectModule', []);

	Effect.config(function ($stateProvider) {
		$stateProvider
			.state('effects', {
				url: '/gear/effects',
				templateUrl: '../../partials/gear/effect.html'
			})
			.state('new-effect', {
				url: '/gear/effect/new',
				templateUrl: '../../partials/gear/new-effect.html'
			})
			.state('edit-effect', {
				url: '/gear/effect/edit/:id',
				templateUrl: '../../partials/gear/edit-effect.html',
				resolve: {
					promiseObject: function ($http, $stateParams) {
						return $http({
							method: 'GET',
							url: '/api/gear/effects/' + $stateParams.id,
							headers: {
								'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
							}
						}).then(function successCallback(response) {
							return response.data;
						});
					}
				},
				controller: function ($scope, promiseObject) {
					$scope.selectedEffect = promiseObject;
				}
			});
	});

	Effect.controller('EffectController', ['$scope', '$http', '$state', '$stateParams', '$window', function ($scope, $http, $state, $stateParams, $window) {
		$http({
			method: 'GET',
			url: '/api/gear/effects',
			headers: {
				'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
			}
		}).then(function successCallback(response) {
			$window.ga('send', 'event', 'Gear Vault', 'List', 'Effect');
			$scope.effects = response.data.effects;
		}, function errorCallback(response) {
			console.error(response);
		});

		$scope.post = function (effect) {
			$http({
				method: 'POST',
				data: effect,
				url: '/api/gear/effects',
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Create', 'Effect');
				$state.go('effects');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.put = function (effect) {
			$http({
				method: 'PUT',
				data: effect,
				url: '/api/gear/effects/' + $stateParams.id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Update', 'Effect');
				$state.go('effects');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.delete = function (effect, index) {
			$http({
				method: 'DELETE',
				url: '/api/gear/effects/' + effect._id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Remove', 'Effect');
				$scope.effects.splice([index], 1);
			}, function errorCallback(response) {
				console.error(response);
			});
		};
	}]);

})();
