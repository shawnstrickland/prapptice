(function () {

	var Guitar = angular.module('GuitarModule', []);

	Guitar.config(function ($stateProvider) {
		$stateProvider
			.state('guitars', {
				url: '/gear/guitars',
				templateUrl: '../../partials/gear/guitar.html'
			})
			.state('new-guitar', {
				url: '/gear/guitar/new',
				templateUrl: '../../partials/gear/new-guitar.html'
			})
			.state('edit-guitar', {
				url: '/gear/guitar/edit/:id',
				templateUrl: '../../partials/gear/edit-guitar.html',
				resolve: {
					promiseObject: function ($http, $stateParams) {
						return $http({
							method: 'GET',
							url: '/api/gear/guitars/' + $stateParams.id,
							headers: {
								'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
							}
						}).then(function successCallback(response) {
							return response.data;
						});
					}
				},
				controller: function ($scope, promiseObject) {
					$scope.selectedGuitar = promiseObject;
				}
			});
	});

	Guitar.controller('GuitarController', ['$scope', '$http', '$state', '$stateParams', '$window', function ($scope, $http, $state, $stateParams, $window) {
		$http({
			method: 'GET',
			url: '/api/gear/guitars',
			headers: {
				'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
			}
		}).then(function successCallback(response) {
			$window.ga('send', 'event', 'Gear Vault', 'List', 'Guitar');
			$scope.guitars = response.data.guitars;
		}, function errorCallback(response) {
			console.error(response);
		});

		$scope.post = function (guitar) {
			$http({
				method: 'POST',
				data: guitar,
				url: '/api/gear/guitars',
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Create', 'Guitar');
				$state.go('guitars');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.put = function (guitar) {
			$http({
				method: 'PUT',
				data: guitar,
				url: '/api/gear/guitars/' + $stateParams.id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Update', 'Guitar');
				$state.go('guitars');
			}, function errorCallback(response) {
				console.error(response);
			});
		};

		$scope.delete = function (guitar, index) {
			$http({
				method: 'DELETE',
				url: '/api/gear/guitars/' + guitar._id,
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback() {
				$window.ga('send', 'event', 'Gear Vault', 'Remove', 'Guitar');
				$scope.guitars.splice([index], 1);
			}, function errorCallback(response) {
				console.error(response);
			});
		};
	}]);

})();
