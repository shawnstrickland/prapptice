	(function () {
		
		var Entry = angular.module('EntryModule', []);
		
		Entry.config(function ($stateProvider) {
		$stateProvider
			.state('entries', {
				url: '/entries',
				templateUrl: '../partials/entry.html'
			})
			.state('new-entry', {
				url: '/entry/new',
				templateUrl: '../partials/new-entry.html'
			})
			.state('edit-entry', {
				url: '/entry/edit/:id',
				templateUrl: '../partials/edit-entry.html',
				resolve: {
					promiseObject: function ($http, $stateParams) {
						return $http({
							method: 'GET',
							url: '/api/entry/' + $stateParams.id,
							headers: {
								'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
							}
						}).then(function successCallback(response) {
							return response.data;
						});
					}
				},
				controller: function ($scope, promiseObject) {
					$scope.selectedEntry = promiseObject;
				}
			});
	});
		
		Entry.controller('EntryController', ['$scope', '$http', '$state', '$stateParams', '$window', function ($scope, $http, $state, $stateParams, $window) {
			$http({
				method: 'GET',
				url: '/api/entry',
				headers: {
					'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
				}
			}).then(function successCallback(response) {
				$window.ga('send', 'event', 'Practice Log', 'List', 'Entry');
				$scope.entries = response.data.entries;
			}, function errorCallback(response) {
				console.error(response);
			});
			
			$scope.post = function (entry) {
				$http({
					method: 'POST',
					data: entry,
					url: '/api/entry',
					headers: {
						'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
					}
				}).then(function successCallback() {
					$window.ga('send', 'event', 'Practice Log', 'Create', 'Entry');
					$state.go('entries');
				}, function errorCallback(response) {
					console.error(response);
				});
			};
			
			$scope.put = function (entry) {
				$http({
					method: 'PUT',
					data: entry,
					url: '/api/entry/' + $stateParams.id,
					headers: {
						'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
					}
				}).then(function successCallback() {
					$window.ga('send', 'event', 'Practice Log', 'Update', 'Entry');
					$state.go('entries');
				}, function errorCallback(response) {
					console.error(response);
				});
			};
			
			$scope.delete = function (entry, index) {
				$http({
					method: 'DELETE',
					url: '/api/entry/' + entry._id,
					headers: {
						'Authorization': localStorage.getItem("token_type") + " " + localStorage.getItem("access_token")
					}
				}).then(function successCallback() {
					$window.ga('send', 'event', 'Practice Log', 'Remove', 'Entry');
					$scope.entries.splice([index], 1);
				}, function errorCallback(response) {
					console.error(response);
				});
			};
		}]);
		
	})();
