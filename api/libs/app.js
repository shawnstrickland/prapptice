var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var methodOverride = require('method-override');

var libs = process.cwd() + '/api/libs/';
require(libs + 'auth/auth');

var config = require('./config');
var log = require('./log')(module);
var oauth2 = require('./auth/oauth2');

var api = require('./routes/api');
var user = require('./routes/user');
var entry = require('./routes/entries');
var gear = require('./routes/gear');
var notes = require('./routes/sight-reading/notes');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(methodOverride());
app.use(passport.initialize());

// Set up static directory for HTML
app.use('/', express.static(process.cwd() + '/frontend'));
app.use('/bower_components', express.static(process.cwd() + '/bower_components'));
app.use('/js', express.static(process.cwd() + '/frontend/js'));
app.use('/styles', express.static(process.cwd() + '/frontend/styles'));
app.use('/images', express.static(process.cwd() + '/frontend/images'));

/* Set up routes */
app.use('/api', api);
app.use('/api/user', user);
app.use('/api/entry', entry);
app.use('/api/gear', gear);
app.use('/api/oauth/token', oauth2.token);
app.use('/api/sight-reading/notes', notes);

app.all('/*', function(req, res, next) {
	// Just send the index.html for other files to support HTML5Mode
	res.sendFile('index.html', { root: process.cwd() + '/frontend' });
});

// catch 404 and forward to error handler
app.use(function(req, res, next){
    res.status(404);
    log.debug('%s %d %s', req.method, res.statusCode, req.url);
    res.json({
    	error: 'Not found'
    });
    return;
});

// error handlers
app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('%s %d %s', req.method, res.statusCode, err.message);
    res.json({
    	error: err.message
    });
    return;
});

module.exports = app;
