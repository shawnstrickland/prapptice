var mongoose = require('mongoose'),
	crypto = require('crypto'),

	Schema = mongoose.Schema,

	User = new Schema({
		email: {
			type: String,
			unique: true,
			required: true
		},
		hashedPassword: {
			type: String,
			required: true
		},
		first_name: {
			type: String,
			required: true
		},
		last_name: {
			type: String,
			required: true
		},
		show_name: {
			type: Boolean,
			required: true
		},
		zipcode: {
			type: Number,
			required: true
		},
		salt: {
			type: String,
			required: true
		},
		created: {
			type: Date,
			default: Date.now
		}
	});

User.methods.encryptPassword = function(password) {
	// NOTE: IF YOU CHANGE THIS, YOUR USERS CAN NOT LOG IN ANYMORE, A MANUAL MIGRATION WILL BE NECESSARY
	return crypto.pbkdf2Sync(password, this.salt, 100000, 512, 'sha512').toString('hex');
};

User.virtual('userId')
.get(function () {
	return this.id;
});

User.virtual('password')
	.set(function(password) {
		this._plainPassword = password;
		this.salt = crypto.randomBytes(128).toString('hex');
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function() { return this._plainPassword; });


User.methods.checkPassword = function(password) {
	return this.encryptPassword(password) === this.hashedPassword;
};

module.exports = mongoose.model('User', User);
