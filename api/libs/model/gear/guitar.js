var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Guitar = new Schema({
	manufacturer: { type: String, required: true },
	model: { type: String, required: true },
	color: { type: String },
	modifications: { type: String },
	serial_number: { type: String },
	value_at_purchase: { type: Number },
	value_at_current: { type: Number, required: true },
	_creator: { type: Schema.ObjectId, required: true, ref: 'User' }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Gear.Guitar', Guitar);
