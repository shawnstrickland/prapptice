var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Entry = new Schema({
	summary: { type: String, required: true },
	duration: { type: Number },
	category: { type: String },
	notes: { type: String },
	_creator: { type: Schema.ObjectId, required: true, ref: 'User' }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('Entry', Entry);
