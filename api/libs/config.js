var nconf = require('nconf');

nconf.argv()
	.env()
	.file({
		file: process.cwd() + '/api/config.json'
	});

module.exports = nconf;
