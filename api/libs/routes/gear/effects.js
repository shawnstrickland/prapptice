var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);

var Effect = require(libs + 'model/gear/effect');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {
	Effect.find({ _creator: req.user._id }).sort({ created_at: 'desc' }).exec(function (err, effects) {
		if (!err) {
			return res.json({ effects: effects });
		} else {
			res.statusCode = 500;
			log.error('Internal error(%d): %s',res.statusCode,err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {
	Effect.findOne({ _id: req.params.id, _creator: req.user._id }).exec(function (err, effect) {
		if (!err) {
			return res.json(effect);
		} else {
			res.statusCode = 500;

			log.error('Internal error(%d): %s',res.statusCode,err.message);

			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {

	var effect = new Effect({
		manufacturer: req.body.manufacturer,
		model: req.body.model,
		modifications: req.body.modifications,
		serial_number: req.body.serial_number,
		value_at_purchase: req.body.value_at_purchase,
		value_at_current: req.body.value_at_current,
		_creator: req.user._id
	});

	effect.save(function (err) {
		if (!err) {
			return res.json({ effect: effect });
		} else {
			res.statusCode = 500;
			return res.json({ message: "Error saving effect.", error: err });
		}
	});

});

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Effect.findOne({ _id: req.params.id, _creator: req.user._id }, function (err, effect) {
		if (!err) {
			effect.manufacturer = req.body.manufacturer;
			effect.model = req.body.model;
			effect.modifications = req.body.modifications;
			effect.serial_number = req.body.serial_number;
			effect.value_at_purchase = req.body.value_at_purchase;
			effect.value_at_current = req.body.value_at_current;
			effect.save(function (err) {
				if (!err) {
					// TODO: Be more descriptive in the log about who did what to what. // Effect UID has been updated by user: UID
					console.log("Effect updated.");
					return res.json({ message: "Effect has been updated.", effect: effect});
				} else {
					console.error("Problem saving changes to effect.");
					return res.json({ message: "Problem saving effect.", error: err });
				}
			});
		} else {
			return console.error(err);
		}
	});

});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Effect.findOneAndRemove({ _id: req.params.id, _creator: req.user._id }, function (err) {
		if (!err) {
			console.log("Effect removed (If sufficient access was provided).");
			return res.json({ message: "Effect removed (If sufficient access was provided)." });
		} else {
			console.log("Error finding and removing effect.");
			return res.json({ message: "Error finding and removing effect.", error: err });
		}
	});

});

module.exports = router;
