var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);

var Guitar = require(libs + 'model/gear/guitar');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {
	Guitar.find({ _creator: req.user._id }).sort({ created_at: 'desc' }).exec(function (err, guitars) {
		if (!err) {
			return res.json({ guitars: guitars });
		} else {
			res.statusCode = 500;
			log.error('Internal error(%d): %s',res.statusCode,err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {
	Guitar.findOne({ _id: req.params.id, _creator: req.user._id }).exec(function (err, guitar) {
		if (!err) {
			return res.json(guitar);
		} else {
			res.statusCode = 500;

			log.error('Internal error(%d): %s',res.statusCode,err.message);

			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {

	var guitar = new Guitar({
		manufacturer: req.body.manufacturer,
		model: req.body.model,
		color: req.body.color,
		modifications: req.body.modifications,
		serial_number: req.body.serial_number,
		value_at_purchase: req.body.value_at_purchase,
		value_at_current: req.body.value_at_current,
		_creator: req.user._id
	});

	guitar.save(function (err) {
		if (!err) {
			return res.json({ guitar: guitar });
		} else {
			res.statusCode = 500;
			return res.json({ message: "Error saving guitar.", error: err });
		}
	});

});

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Guitar.findOne({ _id: req.params.id, _creator: req.user._id }, function (err, guitar) {
		if (!err) {
			guitar.manufacturer = req.body.manufacturer;
			guitar.model = req.body.model;
			guitar.color = req.body.color;
			guitar.modifications = req.body.modifications;
			guitar.serial_number = req.body.serial_number;
			guitar.value_at_purchase = req.body.value_at_purchase;
			guitar.value_at_current = req.body.value_at_current;
			guitar.save(function (err) {
				if (!err) {
					// TODO: Be more descriptive in the log about who did what to what. // Guitar UID has been updated by user: UID
					console.log("Guitar updated.");
					return res.json({ message: "Guitar has been updated.", guitar: guitar});
				} else {
					console.error("Problem saving changes to guitar.");
					return res.json({ message: "Problem saving guitar.", error: err });
				}
			});
		} else {
			return console.error(err);
		}
	});

});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Guitar.findOneAndRemove({ _id: req.params.id, _creator: req.user._id }, function (err) {
		if (!err) {
			console.log("Guitar removed (If sufficient access was provided).");
			return res.json({ message: "Guitar removed (If sufficient access was provided)." });
		} else {
			console.log("Error finding and removing guitar.");
			return res.json({ message: "Error finding and removing guitar.", error: err });
		}
	});

});

module.exports = router;
