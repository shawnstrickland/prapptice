var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);

var Amp = require(libs + 'model/gear/amp');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {
	Amp.find({ _creator: req.user._id }).sort({ created_at: 'desc' }).exec(function (err, amps) {
		if (!err) {
			return res.json({ amps: amps });
		} else {
			res.statusCode = 500;
			log.error('Internal error(%d): %s',res.statusCode,err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {
	Amp.findOne({ _id: req.params.id, _creator: req.user._id }).exec(function (err, amp) {
		if (!err) {
			return res.json(amp);
		} else {
			res.statusCode = 500;

			log.error('Internal error(%d): %s',res.statusCode,err.message);

			return res.json({
				error: 'Server error'
			});
		}
	});
});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {

	var amp = new Amp({
		manufacturer: req.body.manufacturer,
		model: req.body.model,
		color: req.body.color,
		modifications: req.body.modifications,
		serial_number: req.body.serial_number,
		value_at_purchase: req.body.value_at_purchase,
		value_at_current: req.body.value_at_current,
		_creator: req.user._id
	});

	amp.save(function (err) {
		if (!err) {
			return res.json({ amp: amp });
		} else {
			res.statusCode = 500;
			return res.json({ message: "Error saving amp.", error: err });
		}
	});

});

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Amp.findOne({ _id: req.params.id, _creator: req.user._id }, function (err, amp) {
		if (!err) {
			amp.manufacturer = req.body.manufacturer;
			amp.model = req.body.model;
			amp.color = req.body.color;
			amp.modifications = req.body.modifications;
			amp.serial_number = req.body.serial_number;
			amp.value_at_purchase = req.body.value_at_purchase;
			amp.value_at_current = req.body.value_at_current;
			amp.save(function (err) {
				if (!err) {
					// TODO: Be more descriptive in the log about who did what to what. // Amp UID has been updated by user: UID
					console.log("Amp updated.");
					return res.json({ message: "Amp has been updated.", amp: amp});
				} else {
					console.error("Problem saving changes to amp.");
					return res.json({ message: "Problem saving amp.", error: err });
				}
			});
		} else {
			return console.error(err);
		}
	});

});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Amp.findOneAndRemove({ _id: req.params.id, _creator: req.user._id }, function (err) {
		if (!err) {
			console.log("Amp removed (If sufficient access was provided).");
			return res.json({ message: "Amp removed (If sufficient access was provided)." });
		} else {
			console.log("Error finding and removing amp.");
			return res.json({ message: "Error finding and removing amp.", error: err });
		}
	});

});

module.exports = router;
