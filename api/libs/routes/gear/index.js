var router = require('express').Router();

// split up route handling for gear section
router.use('/guitars', require('./guitars'));
router.use('/amps', require('./amps'));
router.use('/effects', require('./effects'));

module.exports = router;
