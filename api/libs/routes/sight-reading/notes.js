var express = require('express');
var passport = require('passport');

var router = express.Router();

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);

var helperFunctions = require('./helper');

var possibleNotesArray = [
	'C1','D1','E1','F1','G1','A1','B1',
	'C2','D2','E2','F2','G2','A2','B2',
	'C3','D3','E3','F3','G3','A3','B3',
	'C4','D4','E4','F4','G4','A4','B4',
	'C5','D5','E5','F5','G5','A5','B5',
	'C6','D6','E6','F6','G6','A6','B6',
	'C7','D7','E7','F7','G7','A7','B7'
];

function boundsConversion(bounds) {
	// Create a new array by slicing the possibleNotesArray to just the gap we need for the possible notes bounds
	// and pass back to the generateRandomNote() as var = randomNote
	return possibleNotesArray.slice(possibleNotesArray.indexOf(bounds[0]), possibleNotesArray.indexOf(bounds[1]) + 1);
}

router.post('/', passport.authenticate('bearer', { session: false }), (req, res) => {
	// :frequency is a sliding scale for every (n) notes, whether to apply the random sharp or flat to it. A larger number will have fewer accidentals

	var selectedNoteRangeArray = boundsConversion(req.body.bounds);
	var noteCount = 0;
	var partialNotationString = "";
	var fullNotationString = "";

	function redrawRhythms() {
		var numberOfNotesToGenerate = req.body.numberOfNotes;
		// Loop through and create random rhythms for a duration of 256 beats
		for (let i = 1; i <= numberOfNotesToGenerate; i++) {
			noteCount++;
			if (i % req.body.frequency === 0) {
				// Each n frequency in :frequency, run generateRandomNote() with frequency adjustment
				partialNotationString += helperFunctions.generateRandomNote(selectedNoteRangeArray, req.body.sharps, req.body.flats);
			}
			function findRightLineBreakNoteLength(value) {
				if (i % value === 0) {
					// Each n note, generate a line break
					// ABCjs uses line breaks to define layout of the notation
					partialNotationString += "\n";
				}
			}
			var appropriateNotesPerLine = 12;
			findRightLineBreakNoteLength(appropriateNotesPerLine);
		}
		fullNotationString = "M:none\nK:" + req.body.key + "\nV:1 clef="+ req.body.clef +"\n" + partialNotationString;
	}

	redrawRhythms();

	return res.json({
		message: fullNotationString
	})
});

module.exports = router;
