// Generate a random whole number between a minimum and maximum, including said numbers
exports.getRandomInteger = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1)) + min
};

exports.generateRandomNote = (selectedNoteRangeArray, sharpsBoolean, flatsBoolean) => {
	function translateToCorrectOctave(selectedNoteRangeArrayItem) {
		// For ABCJS, we need to translate the traditional numbered octaves into a series of ticks and other marks
		var returnString = "";
		var note = selectedNoteRangeArray[randomNote - 1].replace(/[0-9]/g, '');
		var octaveMarker = selectedNoteRangeArrayItem.replace(/\D+/g, '');

		switch (octaveMarker) {
			// If 1, switch to upper case and add three commas
			case "1":
				returnString += note.toUpperCase();
				returnString += ",,,";
				break;
			// If 2, switch to upper case and add two commas
			case "2":
				returnString += note.toUpperCase();
				returnString += ",,";
				break;
			// If 3, switch to upper case and add a comma
			case "3":
				returnString += note.toUpperCase();
				returnString += ",";
				break;
			// If 4, switch to upper case
			case "4":
				returnString += note.toUpperCase();
				break;
			// If 5 switch to lower case
			case "5":
				returnString += note.toLowerCase();
				break;
			//	If 6 add tick
			case "6":
				returnString += note.toLowerCase();
				returnString += "'";
				break;
			//	If 7 add tick
			case "7":
				returnString += note.toLowerCase();
				returnString += "''";
				break;
			default:
				console.error("Problem with translateToCorrectOctave() call.");
		}

		return returnString;
	}

	// Pick a number to represent on/off state of sharpsBoolean and flatsBoolean
	if (sharpsBoolean === true) {
		var randomSharp = exports.getRandomInteger(1,2);
	}
	if (flatsBoolean === true) {
		var randomFlat = exports.getRandomInteger(1,2);
	}

	if (sharpsBoolean === true && flatsBoolean === true) {
		// We need to randomly pick which one we actually want to use
		var randomSharpOrFlat = exports.getRandomInteger(1,2);
		switch (randomSharpOrFlat) {
			// Turn off either sharps or flats for that random note
			case 1:
				sharpsBoolean = null;
				break;
			case 2:
				flatsBoolean = null;
				break;
			default:
				sharpsBoolean = null;
		}
	}

	// Pick a random musical note
	var randomNote = exports.getRandomInteger(1,selectedNoteRangeArray.length);
	// Generate the random string based on the array note bounds given
	var returnString = "";
	if (sharpsBoolean === true) if (randomSharp === 1) returnString += "^";
	if (flatsBoolean === true) if (randomFlat === 1) returnString += "_";
	// returnString += selectedNoteRangeArray[randomNote - 1].toLowerCase().replace(/[0-9]/g, '');
	returnString += translateToCorrectOctave(selectedNoteRangeArray[randomNote - 1]);
	returnString += "0 ";

	return returnString
};
