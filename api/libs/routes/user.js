var express = require('express');
var passport = require('passport');
var router = express.Router();

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);
var User = require(libs + 'model/user');

router.post('/',
	function(req, res) {
		var user = new User({
			email: req.body.email,
			password: req.body.password,
			first_name: req.body.first_name,
			last_name: req.body.last_name,
			show_name: req.body.show_name,
			zipcode: req.body.zipcode
		});

		user.save(function(err, user) {
			if (!err) {
				res.json({ status: "success", message: "User " + user._id + " successfully created." });
			} else {
				res.json({ status: "failure", message: err.errmsg });
				return log.error(err);
			}
		});
	}
);

router.get('/info', passport.authenticate('bearer', { session: false }),
    function(req, res) {
        // req.authInfo is set using the `info` argument supplied by
        // `BearerStrategy`.  It is typically used to indicate scope of the token,
        // and used in access control checks.  For illustrative purposes, this
        // example simply returns the scope in the response.
        res.json({
        	user_id: req.user.userId,
					show_name: req.user.show_name,
					first_name: req.user.first_name,
					last_name: req.user.last_name,
					zipcode: req.user.zipcode,
					email: req.user.email,
        	scope: req.authInfo.scope
        });
    }
);

module.exports = router;
