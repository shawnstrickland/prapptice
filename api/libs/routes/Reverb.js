var https = require('https');
var async = require('async');

Array.prototype.sum = function() {
    return Math.round(this.reduce(function(a,b){return a+b;}))
};

var Reverb = function () {
	this.priceGuide = function (query) {
		https.get("https://reverb.com/api/priceguide?query=" + query, (body) => {
			var data = "";
			body.on('data', (chunk) => {
				return data += chunk
			});
			body.on('end', () => {
				// After we've received all the data we'll find the average
				// TODO: This can be done much better, look into average dollar amounts in javascript
				data = JSON.parse(data);
				var bottomPriceArray = [];
				var topPriceArray = [];
				async.forEach(data.price_guides, function (item, callback) {
					bottomPriceArray.push(item.estimated_value.bottom_price * 100);
					topPriceArray.push(item.estimated_value.top_price * 100);
					callback()
				}, (err) => {
					if (err) console.error(err);
					console.log('worked');
					var bottomPriceAverage = Math.round(bottomPriceArray.sum() / bottomPriceArray.length);
					var topPriceAverage = Math.round(topPriceArray.sum() / topPriceArray.length);
					return +((bottomPriceAverage + topPriceAverage / 2) / 100).toFixed(2)
				});
			});
		}).on('error', (e) => {
			console.log("Got error: " + e.message)
		})
	}
};

module.exports = Reverb;
