var express = require('express');
var passport = require('passport');
var router = express.Router();

var async = require('async');

var libs = process.cwd() + '/api/libs/';
var log = require(libs + 'log')(module);

var Entry = require(libs + 'model/entry');

router.get('/', passport.authenticate('bearer', { session: false }), function(req, res) {
	Entry.find({ _creator: req.user._id }).sort({ created_at: 'desc' }).exec(function (err, entries) {
		if (!err) {
			return res.json({ entries: entries });
			// TODO: May want to paginate these in the case where there are a ton to filter through
			//if (entries.length <= req.params.itemsPerPage) {
			//	return res.json({is_final: true, total_count: entries.length, entries: entries});
			//} else {
			//	var startingIndex, isFinal = false;
			//	if (req.params.page === 1) {
			//		startingIndex = 0;
			//	} else {
			//		startingIndex = (((req.params.page) - 1) * req.params.itemsPerPage);
			//	}
			//	var selectedEntries = entries.splice(startingIndex, req.params.itemsPerPage);
			//	for (var i = 0; i < selectedEntries[selectedEntries.length]; i++) {
			//		if (JSON.stringify(selectedEntries[i]) === JSON.stringify(entries[entries.length])) {
			//			isFinal = true;
			//		}
			//	}
			//	return res.json({is_final: isFinal, total_count: entries.length, entries: selectedEntries});
			//}
		} else {
			res.statusCode = 500;
			log.error('Internal error(%d): %s',res.statusCode,err.message);
			return res.json({
				error: 'Server error'
			});
		}
	});

});

// Grabs the total amount of hours written to the database.  Must be formatted as a number with no pre-fix or post-fix.
router.get('/hours', function (req, res) {

	var hoursArray = [];
	Entry.find().exec(function (err, entries) {
		async.forEach(entries, function (entry, done) {
			hoursArray.push(entry.duration);
			done();
		}, function () {
			var sum = 0;
			for (var i = 0; i < hoursArray.length; i++) {
				sum += hoursArray[i];
			}
			res.json({ hours: sum });
		});
	});

});

router.get('/:id', passport.authenticate('bearer', { session: false }), function(req, res) {

	Entry.findOne({ _id: req.params.id, _creator: req.user._id }).exec(function (err, entry) {
		if (!err) {
			return res.json(entry);
		} else {
			res.statusCode = 500;

			log.error('Internal error(%d): %s',res.statusCode,err.message);

			return res.json({
				error: 'Server error'
			});
		}
	});

});

router.post('/', passport.authenticate('bearer', { session: false }), function (req, res) {

	var entry = new Entry({
		summary: req.body.summary,
		duration: req.body.duration,
		category: req.body.category,
		notes: req.body.notes,
		_creator: req.user._id
	});

	entry.save(function (err) {
		if (!err) {
			return res.json({ entry: entry });
		} else {
			res.statusCode = 500;
			return res.json({ message: "Error saving entry.", error: err });
		}
	});

});

router.put('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Entry.findOne({ _id: req.params.id, _creator: req.user._id }, function (err, entry) {
		if (!err) {
			entry.summary = req.body.summary;
			entry.duration = req.body.duration;
			entry.category = req.body.category;
			entry.notes = req.body.notes;
			entry.save(function (err) {
				if (!err) {
					// TODO: Be more descriptive in the log about who did what to what. // Entry UID has been updated by user: UID
					console.log("Entry updated.");
					return res.json({ message: "Entry has been updated.", entry: entry});
				} else {
					console.error("Problem saving changes to entry.");
					return res.json({ message: "Problem saving entry.", error: err });
				}
			});
		} else {
			return console.error(err);
		}
	});

});

router.delete('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {

	Entry.findOneAndRemove({ _id: req.params.id, _creator: req.user._id }, function (err) {
		if (!err) {
			console.log("Entry removed (If sufficient access was provided).");
			return res.json({ message: "Entry removed (If sufficient access was provided)." });
		} else {
			console.log("Error finding and removing entry.");
			return res.json({ message: "Error finding and removing entry.", error: err });
		}
	});

});


module.exports = router;
