# Prapptice REST API

REST Web API and application for practice logging for musicians.

## Running project

You'll need NodeJS installed.

### Install dependencies 

To install dependencies enter project folder and run following command:
```
npm install && bower install
```

### Run server	

To run server execute:
```
node api/bin/www 
```
